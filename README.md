# Proyecto Backend Programacion Web - ISTEA

Parte 2 del proyecto donde se realiza un backend con WebApi en .net

Para armar la migracion:
dotnet ef migrations add Inicial --project Persistencia/Persistencia.csproj --startup-project WebApi/WebApi.csproj

Para database
dotnet ef database update --project Persistencia/Persistencia.csproj --startup-project WebApi/WebApi.csproj
