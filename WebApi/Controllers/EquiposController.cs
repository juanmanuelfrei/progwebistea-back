using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;

using Persistencia.Models;
using Persistencia;

namespace WebApi.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class EquiposController : ControllerBase
    {

        private readonly TorneoDbContext _context;

        public EquiposController(TorneoDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<Equipo> GetEquipos()
        {
            return  _context.Equipo.ToList();
        }

        [HttpGet("{id}")]
        public Equipo GetEquipoById(int id)
        {
            var equipoById = _context.Equipo.Where( equipo => equipo.Id == id).FirstOrDefault();
            return equipoById;
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteEquipo(int id)
        {
            var equipo = _context.Equipo.Where( equipo => equipo.Id == id).FirstOrDefault();
            _context.Equipo.Remove(equipo);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("altaequipo")]
        public void CrearEquipo(Equipo equipo)
        {
            _context.Equipo.Add(equipo);
            _context.SaveChanges();
        }



    }
}