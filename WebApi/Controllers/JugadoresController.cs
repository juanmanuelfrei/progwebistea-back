using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;

using Persistencia.Models;
using Persistencia;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JugadoresController : ControllerBase
    {

        private readonly TorneoDbContext _context;

        public JugadoresController(TorneoDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<Jugador> GetJugadores()
        {
            return _context.Jugador.ToList();
        }

        [HttpGet("{id}")]
        public Jugador GetJugadorById(int id)
        {
            var jugadorById = _context.Jugador.Where(jugador => jugador.Id == id).FirstOrDefault();
            return jugadorById;
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteJugador(int id)
        {
            var jugador = _context.Jugador.Where(jugador => jugador.Id == id).FirstOrDefault();
            _context.Jugador.Remove(jugador);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("actualizajugador/gol")]
        public IActionResult actualizarGoles(int id, string accion)
        {

            var jugador = this.GetJugadorById(id);
                
            if (accion == "sumar")
                jugador.Goles++;
            else if(accion == "restar") {
                if (jugador.Goles == 0)
                {
                    return NotFound("No se le pueden restar mas goles a este jugador");
                }
                jugador.Goles--;
            } else {
                return NotFound("Error en informacion enviada");
            }


            _context.SaveChanges();
            return Ok();
        }

        // [HttpPost]
        // [Route("actualizajugador")]
        // public void ActualizarJugador()
        // {

        // }

        [HttpPost]
        public async Task<ActionResult<Jugador>> CrearJugador(Jugador jugador)
        {
            _context.Jugador.Add(jugador);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetJugadorById", new { id = jugador.Id }, jugador);
        }


    }
}