using System;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Persistencia.Models;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;

namespace Persistencia
{
    public partial class TorneoDbContext : DbContext
    {
        public TorneoDbContext() { }

        public DbSet<Equipo> Equipo { get; set; }
        public DbSet<Equipo> Equipos { get; set; }
        public DbSet<Jugador> Jugador { get; set; }
        public DbSet<Usuario> Usuario { get; set; }

        public DbSet<Jugador> Jugadores { get; set; }

        public TorneoDbContext(DbContextOptions<TorneoDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }


    }
}