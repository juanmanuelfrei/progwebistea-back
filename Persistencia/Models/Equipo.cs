using System.Collections.Generic;

namespace Persistencia.Models
{
    public class Equipo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Posicion { get; set; }
        public int Puntos { get; set; }
        public string FechaCreacion { get; set; }
        public bool Activo { get; set; }
        public List<Jugador> Jugadores { get; set; }
    }
}