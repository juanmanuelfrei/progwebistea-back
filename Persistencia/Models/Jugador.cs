namespace Persistencia.Models
{
    public class Jugador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Dorsal { get; set; }
        public int Goles { get; set; }
        public Equipo Equipo { get; set; }
        public string Fecha { get; set; }
        
    }
}